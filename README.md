# 程式碼規範

## 工具及框架
* 源掃軟體：sonarQube
* 主要框架：SpringBoot + JPA
* 選用框架：Apache Camel , Apache Kafka

## SpringBoot 流程
```mermaid
sequenceDiagram
Front End->>Controller: 前端呼叫指定的 URL
Controller->>Service: 呼叫指定的 Service Interface
Service->>Repository: 呼叫指定的 Repository Interface
Repository-->>Service: 回覆 Entity 或 DTO
Service-->>Controller: 回覆還輯處理結果
Controller-->>Front End: 回覆前端請求
```

## 基本語法

> * [基本語法：類別的內容，應依 變數、建構式、方法 的順序進行定義（ `SonarQube` ）](#BASE_1_1)
> * [基本語法：註解的程式碼應直接移除，備註說明除外（ `SonarQube` ）](#BASE_1_2)
> * [基本語法：句子應明確分行撰寫（ `SonarQube` ）](#BASE_1_3)
> * [基本語法：變數不應在被宣告後，就立即被回傳（ `SonarQube` ）](#BASE_1_4)
> * [基本語法：覆寫或實作的方法需明確標記( annotation )（ `SonarQube` ）](#BASE_1_5)
> * [基本語法：巢狀不可過深，超過三層( 包含 "if" , "for" , "while" , "switch" and "try" )（ `SonarQube` ）](#BASE_1_6)
> * [基本語法：字串是否相等判斷語法](#BASE_1_7)
> * [例外處理：公開的方法不可丟擲超過一個的例外（ `SonarQube` ）](#EXCEPTION_1_1)
> * [例外處理：一般的例外應不可直接向外丟擲（ `SonarQube` ）](#EXCEPTION_1_2)

## JDK 8 語法
> * [基本語法：陣列是否為空判斷語法（ `SonarQube` ）](#BASE_1_8)
> * [JDK 8 語法：JDK 8 以上，應使用 LocalDate , LocalDateTime，避免使用 Date 及 SimpleDateFormat](#JDK8_1_1)
>   * [JDK 8 語法：LocalDate , LocalDateTime 基本使用](#JDK8_1_1)
>   * [JDK 8 語法：LocalDate , LocalDateTime 格式轉換](#JDK8_1_2)
>   * [JDK 8 語法：LocalDate , LocalDateTime 格式檢查](#JDK8_1_3)
>   * [JDK 8 語法：LocalDate , LocalDateTime 起迄區間處理](#JDK8_1_4)

## 框架強調
> * [強調部份：Logger 使用 MessageFormat 方式](#LOG_1_1)
> * [強調部份：Logger 使用 error trace 方式](#LOG_1_2)
> * [強調部份：pom.xml 注意事項](#POM_1_1)
>   * [強調部份：一定要宣告 finalName](#POM_1_1)
>   * [強調部份：避免有重複的 dependency](#POM_1_2)

***
### <a name="BASE_1_1">基本語法：類別的內容，應依 變數、建構式、方法 的順序進行定義</a>
`SonarQube : The members of an interface declaration or class should appear in pre-defined order`
> **Move this variable to comply with Java Code COnventions.**

#### 正確範例
```java
public class Person {

  // 變數宣告
  private String name ;

  private int age ;

  // 建構式宣告
  public Person() {

  }

  // 方法宣告
  public String getName() {
      return name ;
  }

  public void setName(String name) {
      this.name = name ;
  }

}

```

#### 錯誤範例
```java
public class Person {

  // 建構式宣告
  public Person() {

  }

  // 變數宣告
  private String name ;

  private int age ;

  // 方法宣告
  public String getName() {
      return name ;
  }

  public void setName(String name) {
      this.name = name ;
  }

}

```

***
### <a name="BASE_1_2">基本語法：註解的程式碼應直接移除，備註說明除外</a>
`SonarQube : Sections of code should not be "comment out"`
> **This block of commented-out lines of code should be removed.**

#### 錯誤範例
```java
public class SomeService {

  //Autowired
  //private ObjectMapper objectMapper ;

  public SomeResponse doSomething(SomeRequest request) {
    SomeResponse response = new SomeResponse();

    ...(省略)

    //String type = "A" ;
    
    ...(省略)

    return response;
  }
  
```

#### 正確範例（承上）
```java
public class SomeService {

  public SomeResponse doSomething(SomeRequest request) {
    SomeResponse response = new SomeResponse();

    ...(省略)

    return response;
  }
  
```

***
### <a name="BASE_1_3">基本語法：句子應明確分行撰寫</a>
`SonarQube : Statements should be on separate lines`

#### 錯誤範例
```java

  if(baseResponse != null) baseResponse.getHeader().setTxnSeq(request.getHeader("TxnSeq"));

```

#### 正確範例（承上）
```java

  if(baseResponse != null) {
    baseResponse.getHeader().setTxnSeq(request.getHeader("TxnSeq"));
  } 

```

***
### <a name="BASE_1_4">基本語法：變數不應在被宣告後，就立即被回傳</a>
`SonarQube : Local Variables should not be declared and then immediately returned or thrown`

#### 錯誤範例
```java
@Controller
public class QueryController {

  @Autowired
  QueryService queryService ;

  public QueryResponse query(QueryRequest request) {
    QueryResponse response = queryService.query(request);
    return response;
  }

}
```

#### 正確範例（承上）
```java
@Controller
public class QueryController {

  @Autowired
  QueryService queryService ;

  public QueryResponse query(QueryRequest request) {
    return queryService.query(request);
  }

}
```

***
### <a name="BASE_1_5">基本語法：覆寫或實作的方法需明確標記( annotation )</a>
`SonarQube : Add the "@override" annotation above this method signature `

#### 範例
```java

  @override
  public void doSomething() {

  }

```

***
### <a name="BASE_1_6">基本語法：巢狀不可過深，超過三層( 包含 "if" , "for" , "while" , "switch" and "try" )</a>
`SonarQube : Control flow Statements "if" , "for" , "while" , "switch" and "try" should not be nested too deeply`

#### 錯誤範例
```java

  public void doSomething(QueryRequest request) throwns CustException {

    try {
      for( Channel channel : request.getChannels() ) {
        for( String prod : channel.getProds() ) {
          if( "ALL".equals( prod ) ) {
            throw new CustException("產品別不可為 ALL");
          }
        }
      }
    }catch(Exception e) {
      log.error(e.toString(), e);
      throw e ;
    }

  }

```

#### 正確範例（承上）
```java
  public void doSomething(QueryRequest request) throwns CustException {

    try {
      for( Channel channel : request.getChannels() ) {
        for( String prod : channel.getProds() ) {
          checkProd( prod ) ;
        }
      }
    }catch(Exception e) {
      log.error(e.toString(), e);
      throw e ;
    }
  }

  private void checkProd(String prod) throws CustException {
    if( "ALL".equals( prod ) ) {
      throw new CustException("產品別不可為 ALL");
    }
  }


```

***
### <a name="BASE_1_7">基本語法：字串是否相等判斷語法</a>

#### 錯誤範例
```java

  if(type.equals("ALL")) {
    ...(省略)
  }

```

#### 正確範例（承上）
```java
  
  if("ALL".equals(type)) {
    ...(省略)
  }

```

***
### <a name="EXCEPTION_1_1">例外處理：公開的方法不可丟擲超過一個的例外</a>
`SonarQube ： Public methods should throw at most one checked exception`
> **應統一處理錯誤，再明確定義並處理例外**

#### 錯誤範例
```java

  public void doSomething(QueryRequest request) throws URISyntaxException, JsonMappingException {

    ...(省略)

  } 

```

#### 正確範例
```java
  
  public void doSomething(QueryRequest request) throws CustException {

    try {

    } catch(CustException e) {
      log.error("預期發生的錯誤" , e);
      throw e ; 
    } catch(Exception e) {
      log.error(e.toString(), e);
      throw CustException("未預期的錯誤") ;
    }
    ...(省略)

  }

```

***
### <a name="EXCEPTION_1_2">例外處理：一般的例外應不可直接向外丟擲</a>
`SonarQube ： Generic exceptions should never be throw`
> **應統一處理錯誤，再明確定義並處理例外**

#### 錯誤範例
```java

  public void doSomething(QueryRequest request) throws Exception {

    ...(省略)

  } 

```

#### 正確範例
```java
  
  public void doSomething(QueryRequest request) throws CustException {

    try {
      ...(省略)
    } catch(CustException e) {
      log.error("預期發生的錯誤" , e);
      throw e ; 
    } catch(Exception e) {
      log.error(e.toString(), e);
      throw CustException("未預期的錯誤") ;
    }
    ...(省略)

  }

```

***
### <a name="BASE_1_8">基本語法：陣列是否為空判斷語法</a>

#### 錯誤範例
```java

  if( itemList != null && itemList.size() > 0 ) {
    ...(省略)
  }

```

#### 正確範例
```java
  
  if( itemList != null && !itemList.isEmpty() ) {
    ...(省略)
  }

```

#### 正確範例（使用 apache）
> org.apache.commons.Collections.CollectionUtils
```java
  
  if( CollectionUtils.isNotEmpty(itemList) ) {
    ...(省略)
  }

```

***
### <a name="JDK8_1_1">JDK 8 語法：LocalDate , LocalDateTime 基本使用</a>

#### 使用範例
```java
public class LocalDateExample {

  public static void main(String[] args) {
    //日期（不含時間）
    LocalDate localDate = LocalDate.now();
    System.out.println("日期（不含時間）:" + localDate);

    //日期（不含時間）
    LocalDateTime localDateTime = LocalDateTime.now();
    System.out.println("日期（包含時間）:" + localDateTime);
  }

}

```

#### 輸出結果
```
日期（不含時間）: 2020-11-11
日期（包含時間）: 2020-11-11T10:30:08.005
```

***
### <a name="JDK8_1_2">JDK 8 語法：LocalDate , LocalDateTime 格式轉換</a>

#### 使用範例
```java
public class LocalDateExample {

  public static void main(String[] args) {
    //字串轉日期
    String yyyyMMdd = "2020-02-28" ;
    DateTimeFormatter dtFmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    System.out.println("字串轉日期：" + LOcalDate.parse(yyyyMMdd, dfFmt) );

    //日期（不含時間）
    System.out.println("日期轉字串：" + dtFmt.format(LocalDate.now());
  }

}

```

#### 輸出結果
```
字串轉日期 : 2020-02-28
日期轉字串 : 2020-11-11
```

***
### <a name="JDK8_1_3">JDK 8 語法：LocalDate , LocalDateTime 格式檢查</a>

#### 使用範例
```java
public class LocalDateExample {

  public static boolean validate(String sDate, String fmt) {
    try {
      DateTimeFormatter dtFmt = DateTimeFormatter.ofPattern(fmt).withResolverStyle(ResolverStyle.STRICT);
      LocalDate.parse(sDate, dtFmt);
      return true ;
    }catch(DateTimeParseException e) {
      log.error(e.toString(), e);
      return false ;
    }
  }

  public static void main(String[] args) {
    System.out.println("日期格式檢查 : " + validate("2020-02-28", "uuuu/MM/dd")); 
    System.out.println("日期格式檢查 : " + validate("2020-02-30", "uuuu/MM/dd")); 
  }

}

```

#### 輸出結果
```
日期格式檢查 : true
日期格式檢查 : false
```

***
### <a name="JDK8_1_4">JDK 8 語法：LocalDate , LocalDateTime 起迄區間處理</a>

#### 使用範例
```java
public class LocalDateExample {

  private static DateTimeFormatter dtFmt = DateTimeFormatter.ofPattern("yyyy-MM-dd");

  public static void main(String[] args) {
    String sDate = "2020-01-01" ;
    String eDate = "2020-03-31" ;

    LOcalDate sLocalDate = LocalDate.parse(sDate, dtFmt) ;
    LOcalDate eLocalDate = LocalDate.parse(eDate, dtFmt) ;

    LocalDateTime sLocalDateTime = LocalDateTime.of(sLocalDate , LocalTime.MIN) ;
    LocalDateTime eLocalDateTime = LocalDateTime.of(eLocalDate , LocalTime.MAX) ;
    
    System.out.println("開始時間：" + sLocalDateTime);
    System.out.println("結束時間：" + eLocalDateTime);
  }

}

```

#### 輸出結果
```
開始時間：2020-01-01T00:00
結束時間：2020-03-31T23:59:59.999999999
```

***
### <a name="LOG_1_1">強調部份：Logger 使用 MessageFormat 方式</a>

#### 錯誤範例
```java

  log.debug(" someone = " + name1 + " ; another = " + name2) ;

  log.info(" someone = " + name1 + " ; another = " + name2) ;

```

#### 正確範例
```java
  
  log.debug(" someone = {} ; another = {}" , name1 , name2) ;

  log.info(" someone = {} ; another = {}" , name1 , name2) ;

```

***
### <a name="LOG_1_2">強調部份：Logger 使用 error trace 方式</a>

#### 錯誤範例
```java

  logger.error(e.toString());

```

#### 正確範例
```java
  
  logger.error(e.toString(), e);

```

***
### <a name="POM_1_1">強調部份：一定要宣告 finalName</a>

#### 範例
```xml
  <build>
    <finalName>your-project-name</finalName>
    <plugins>
      <plugin>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
      </plugin>
    </plugins>
  </build>

```

***
### <a name="POM_1_2">強調部份：避免有重複的 dependency</a>

#### 範例
```xml
  <parent>
    <groupId>org.springframework.boot</groupId>
    <artifactId>xxx-xxx-xxx</artifactId>
    <version>1.0.0</version>
  </parent>

  <dependencies>
    ( 檢查是否有重複宣告的 dependency，含 parent 部份 )
  </dependencies>

```
